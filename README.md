**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.What's more, you could even borrow money if you have bad credit, as our lenders do not automatically disqualify applicants based solely on their credit score.

If approved, the lender will then aim to deposit the funds by direct deposit as soon as on the next business day - a quick solution if you're in an emergency.A loan obtained through our network comes in the form of a legitimate payday loan [which is a type of an unsecured personal loan](https://getsamedaypaydayloan.com/bad-credit-same-day-direct-lender-loan/) between $100 and $1,000, depending on the amount you request, your financial situation and the lender's decision.

The lender will then attempt to take repayment on the agreed-upon date - usually within 30 days (on or just after your next paycheck).
Who Are These Loans For?

We all face unforeseen financial emergencies and it can be extremely stressful if you don't have any savings or a credit card to cover the costs.

Fortunately, whether you need to get the car fixed, your children need new clothes, you need to pay an unexpected bill, or you need to purchase something by gave already spent this month's wage - we're ready to try and connect you with a reputable lender.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

If you are searching for payday loans online same day, then you probably need cash fast.

The good news:

If we connect you with a lender, you can complete their application on the very next page and you will know within an hour (often sooner) if you have been approved.

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).